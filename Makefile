# ecrlogin:
# 	eval `aws ecr get-login --no-include-email`

# sshagent:
# 	eval `ssh-agent -s`

up: #ecrlogin sshagent
	docker-compose up

upd: #ecrlogin sshagent
	docker-compose up -d

upb: #ecrlogin sshagent
	docker-compose up -d --build

stop:
	docker-compose stop

down:
	docker-compose down