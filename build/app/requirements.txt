flask==1.0.2
Flask-Bootstrap==3.3.7.1
Flask-WTF==0.14.2
WTForms==2.2.1
requests==2.18.4
pymongo==3.8.0
jsonschema==2.6.0