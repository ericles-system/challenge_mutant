from database import get_db
from datetime import datetime, timedelta
from config import COLLECTION_NAME
class User(object):

    def __init__(self):
        '''

        '''
        self.created_date = datetime.utcnow()
        self.db = get_db()

    def insert(self, collection_name=COLLECTION_NAME, **kargs):
        obj_id = None
        try:
            data = self.json(**kargs)
            if not self.db[collection_name].find_one({"email": data['email']}):
                obj_id = self.db[collection_name].insert_one(data).inserted_id
        except:
            raise                

        return obj_id

    def find_one(self, collection_name=COLLECTION_NAME):
        user = None
        try:
            user = self.db[collection_name].find_one()
        except:
            raise

        return user

    def find(self, collection_name=COLLECTION_NAME):
        user = []
        try:
            for u in self.db[collection_name].find():
                localtime = u['created_in'] - timedelta(hours=3)
                user.append({
                    'id': str(u['_id']),
                    'name': u['name'],
                    'email': u['email'],
                    'website': u['website'],
                    'company': u['company'],
                    'created_in': datetime.strftime(localtime, '%d/%m/%Y')
                })
        except:
            pass

        return user
        
    def json(self, **kargs):
        created = {'created_in': self.created_date}
        return {
                **kargs,
                **created
            }