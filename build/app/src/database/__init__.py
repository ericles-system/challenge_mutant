import pymongo
from config import (
    MONGO_HOST,
    MONGO_PORT,
    COLLECTION_NAME,
    MONGO_DATABASE
)

def get_db():
    client = pymongo.MongoClient(MONGO_HOST, int(MONGO_PORT))
    db = client[MONGO_DATABASE]
    return db