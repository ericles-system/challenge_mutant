$(document).on('click','#btn-export', docCSV);
            
function downloadCSV(csv, filename) {
    var csvFile;
    var downloadLink;

    csvFile = new Blob([csv], {type: "application/csv"});
    downloadLink = document.createElement("a");
    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.click();
}

function tableToCSV(table) {
    var csv = [];
    var rows = table.querySelectorAll("tr");
    
    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        
        for (var j = 0; j < cols.length; j++) 
            row.push(cols[j].innerText.trim());
        
        csv.push(row.join(","));        
    }
    
    return csv.join("\n");
}

function docCSV(){
    var d = new Date();
    var n = d.toISOString();
    var table = document.querySelector('#current table')
    var filename = 'users_api' + '_' + n +'.csv';

    var csv = tableToCSV(table);

    // Download CSV file
    downloadCSV(csv, filename);
}