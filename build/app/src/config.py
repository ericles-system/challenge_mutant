from lib_formatter_logger import log
from lib_formatter_logger.utils import config_validator

logger = log.getLogger('mutant')

variables = [
    { "name": "MONGO_HOST", "required": True },
    { "name": "MONGO_PORT", "default": 27017, "type": "int" },
    { "name": "MONGO_DATABASE", "required": True },
    { "name": "COLLECTION_NAME", "required": True },
    { "name": "URL_API", "required": True },
]

try:
    ( # pylint: disable=E0632
      MONGO_HOST
    , MONGO_PORT
    , MONGO_DATABASE
    , COLLECTION_NAME
    , URL_API
    ) = config_validator(variables)
except:
    logger.exception("Can't continue due to error in configuration")
    exit(1)

PATH_USER='/users'