import requests
import json
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

class RequestApi(object):
    
    def __init__(self, url, path, method, body=None):
        '''
            Constructor Method

            Arguments:
                url {str}
                path {str}
                method {str}
                body {dict} -- default None

        '''

        self.body = body
        self.url = url
        self.path = path
        self.method = method

    @staticmethod
    def _requests_retry_session(
        retries=None,
        backoff_factor=0.3, 
        status_forcelist=(500, 502, 503, 504),
        session=None,
    ):
        session = session or requests.Session()
        retry = Retry(
            total=retries,
            read=retries,
            connect=retries,
            backoff_factor=backoff_factor, # A backoff factor to apply between attempts.
            status_forcelist=status_forcelist, # A set of HTTP status codes that we should force a retry on.
        )
        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session
    
    @staticmethod
    def _response(self, response):
        uri = self.url + self.path
        if response.status_code >= 500:
            print('[!] [{0}] Server Error'.format(response.status_code))
            return None
        elif response.status_code == 404:
            print('[!] [{0}] URI not found: [{1}]'.format(response.status_code, uri))
            return None  
        elif response.status_code == 401:
            print('[!] [{0}] Authentication Failed'.format(response.status_code))
            return response.status_code
        elif response.status_code == 400:
            print('[!] [{0}] Bad Request'.format(response.status_code))
            return None
        elif response.status_code >= 300:
            print('[!] [{0}] Unexpected Redirect'.format(response.status_code))
            return None
        elif response.status_code == 200:
            print('[v] [{0}] Request returned with successfully'.format(response.status_code))
            return json.loads(response.content.decode('utf-8'))
        elif response.status_code == 201:
            print('[v] [{0}] Request returned with successfully'.format(response.status_code))
            return json.loads(response.content.decode('utf-8'))
        else:
            print('[?] Unexpected Error: [HTTP {0}]: Content: {1}'.format(response.status_code, response.content))

        return None

    def get_data(self, retries, response=False, timeout=20):
        '''
            Arguments
                retries {int} -- number of attempts will be made via HTTP if the server does not respond.
                token {str}
                response {bool} -- default False
                timeout {int} -- default 20 seconds
            
            Return Description

                if the response is informed as True, it will have 3 possible returns:

                    1 - if the status code does not match any of the methods http:
                        >= 500, == 404, == 401, == 400,> = 300, == 200, == 201: the return will be None.

                    2 - if the status code is == 401, 401 will be returned

                    3 - if the return is == 200 and 201, the json object will be decoded and converted to dictionary.

            Return
                status_code {int} or response {dict}
        '''            
        if str(self.method).upper() != 'GET':
            raise ValueError('error to method, valid only method GET to this function')

        headers = {
            'content-type': "application/json"
        }
        uri = self.url + self.path
        print('Doing GET -> {0}'.format(uri))
        req = (
            self._requests_retry_session(retries=retries)
            .request(self.method, url=uri, timeout=timeout, headers=headers)
        )
        if response:
            return self._response(self, req)

        return req    

    def post_data(self, retries, response=False, timeout=20):
        '''
            Arguments
                retries {int} -- number of attempts will be made via HTTP if the server does not respond.
                token {str}
                response {bool} -- default False
                timeout {int} -- default 20 seconds
            
            Return Description

                if the response is informed as True, it will have 3 possible returns:

                    1 - if the status code does not match any of the methods http:
                        >= 500, == 404, == 401, == 400,> = 300, == 200, == 201: the return will be None.

                    2 - if the status code is == 401, 401 will be returned

                    3 - if the return is == 200 and 201, the json object will be decoded and converted to dictionary.

            Return
                status_code {int} or response {dict}
        '''
        if str(self.method).upper() != 'POST':
            raise ValueError('error to method, valid only method POST to this function')
        if not self.body:
            raise ValueError('Body not informed this function')
        if not isinstance(self.body, dict):
            raise ValueError('the body needs to be informed as a dictionary: body received', self.body)

        headers = {
            'content-type': "application/json"
        }
        uri = self.url + self.path
        print('Doing POST -> {0}'.format(uri))
        req = (
            self._requests_retry_session(retries=retries)
            .request(self.method, url=uri, json=self.body, timeout=timeout, headers=headers)
        )
        if response:
            return self._response(self, req)

        return req    
