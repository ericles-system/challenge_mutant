from flask import Blueprint, abort, render_template, request
from common.consumerapi import RequestApi
from model.user import User
from config import URL_API, PATH_USER
from lib_formatter_logger import log
logger = log.getLogger('mutant')

bp = Blueprint('users', __name__, url_prefix='/users')

@bp.route('/', methods=['GET'])
def users():
    try:
        users = ( 
            RequestApi(url=URL_API, path=PATH_USER, method='GET')
            .get_data(retries=5, response=True)
        )
    except Exception:
        logger.exception('Consult users api failed', exc_info=True)
        return abort(500, 'Problema ao listar Usuarios')
    
    user_bd = User()
    # filtering only user data with suite at address.
    users = [u for u in users if 'suite' in str(u['address']['suite']).lower()]
    try:
        # include user of API on Database
        for u in users:
            data = {}
            data = {
                'id': u['id'],
                'name': u['name'],
                'email': u['email'],
                'website': u['website'],
                'company': u['company']['name']
            }
            obj_id = user_bd.insert(**data)
            if obj_id:
                logger.debug('Obj_id user insert now: {0}'.format(obj_id))
            else:
                logger.debug('user of email: {0} already exists'.format(data['email']))
    except Exception:
        logger.exception('Insert user failed', exc_info=True)
        return abort(500, 'Problema ao inserir Usuarios no banco de dados')

    logger.info('returning data users on frontend')
    return render_template(
        "users.html",
        users=user_bd.find()  
    )

@bp.route('/create', methods=['GET', 'POST'])
def create_user():

    if request.method == 'GET':
        return render_template("form.html")

    parameters = {
        'name': str(request.form.get('name')) + ' ' + str(request.form.get('sobrenome')),
        'email': request.form.get('email'),
        'website': request.form.get('website'),
        'company': request.form.get('company')
    }

    try:
        new_user = User()
        obj_id = new_user.insert(**parameters)
        if obj_id:
            logger.debug('Obj_id user insert now: {0}'.format(obj_id))
        else:
            logger.debug('user of email: {0} already exists'.format(parameters['email']))
    except Exception: 
        logger.exception('Insert user failed', exc_info=True)
        pass
    else:
        return render_template('success.html', msg='Usuário criado com sucesso!')
    
    return render_template('error.html', msg='Falhou ao criar usuário!')
    
def configure(app):
    app.register_blueprint(bp)
