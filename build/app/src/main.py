import os
from common.util import CustomJSONEncoder
from flask import Flask

# Import my views
from views.user import configure as users
class MyConfig(object):
    RESTFUL_JSON = {
        'cls': CustomJSONEncoder
    } # add whatever settings here

    @staticmethod
    def init_app(app):
        app.config['RESTFUL_JSON']['cls'] = app.json_encoder = CustomJSONEncoder
        app.config['PROFILE'] = False

app = Flask(__name__)
app.config.from_object(MyConfig)
MyConfig.init_app(app)

# configure app 
users(app)

if __name__ == "__main__":
    #from werkzeug.contrib.profiler import ProfilerMiddleware
    #app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[50])
    port = os.getenv('PORT', '5000')  # pylint: disable=C0103
    host = os.getenv('HOST', '0.0.0.0')  # pylint: disable=C0103
    debug = os.getenv('DEBUG', 'FALSE') == 'TRUE'  # pylint: disable=C0103

    app.run(host=host, port=int(port), debug=debug)