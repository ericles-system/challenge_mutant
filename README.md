# WEB DEMO APPLICATION - FULL STACK PROJECT

## RESOURCES/REQUIRED

* Python 3.6
* Mongo 4.0.4
* nginx 1.13
* Docker version 18.09.6, build 481bc77
* docker-compose version 1.21.2, build a133471
* uWSGI
  
## CONFIGURATION

- DESCRIPTION OF WHICH EACH MAKEFILE COMMAND DOES

```bash
# to raise all the containers
make up
# to upload all the containers, however run in mode demon (background)
make upd
# to upload all the containers, build the images and execute 
# in mode demon (background)
make upb
# stop all containers
make stop
# Stops containers and remove containers, networks, volumes, 
# and images created by up.
make down
```

**obs: This makefile file is very simplistic, but the idea is to generate the images and at the same time to deploy.

## MODEL/STRUCTURE

## Flask Framework

**This application was developed to meet the convention used in Flask (according to this documentation)**

http://flask.pocoo.org/docs/1.0/patterns/appfactories/

The difference is that when running in docker, as there were some problems in setting `create_app`, I chose not to create it.

    .
    ├── build
    │   ├── app
    │   │   ├── Dockerfile
    │   │   ├── requirements.txt
    │   │   └── src
    │   │       ├── common
    │   │       │   ├── consumerapi.py
    │   │       │   ├── __init__.py
    │   │       │   └── util.py
    │   │       ├── config.py
    │   │       ├── database
    │   │       │   └── __init__.py
    │   │       ├── lib_formatter_logger
    │   │       │   ├── autosetup.py
    │   │       │   ├── config.py
    │   │       │   ├── exceptions.py
    │   │       │   ├── __init__.py
    │   │       │   ├── log.py
    │   │       │   ├── time.py
    │   │       │   └── utils.py
    │   │       ├── main.py
    │   │       ├── model
    │   │       │   ├── __init__.py
    │   │       │   └── user.py
    │   │       ├── static
    │   │       │   ├── css
    │   │       │   │   ├── asc.gif
    │   │       │   │   ├── bg.gif
    │   │       │   │   ├── blue.zip
    │   │       │   │   ├── desc.gif
    │   │       │   │   └── style.css
    │   │       │   ├── img
    │   │       │   └── js
    │   │       │       ├── exportcsv.js
    │   │       │       ├── jquery-latest.js
    │   │       │       ├── jquery.tablesorter.min.js
    │   │       │       └── scripts.js
    │   │       ├── templates
    │   │       │   ├── base.html
    │   │       │   ├── error.html
    │   │       │   ├── form.html
    │   │       │   ├── success.html
    │   │       │   └── users.html
    │   │       ├── uwsgi.ini
    │   │       └── views
    │   │           ├── __init__.py
    │   │           └── user.py
    │   ├── nginx
    │   │   ├── default.conf
    │   │   └── default.config
    │   └── web
    │       ├── Dockerfile
    │       ├── index.html
    │       └── style.css
    ├── doc
    │   └── Teste Fullstack - CXAI.pdf
    ├── docker-compose.yml
    ├── envs
    │   └── envs.env
    ├── Makefile
    └── README.md


### PROXY

In the containers was created the option of having a reverse proxy with nginx, allowing only the backend to obtain access to all containers.

volume arrow to: `./build/nginx/default.conf:/etc/nginx/conf.d/default.conf`

## LIBS

### BACKEND

* flask==1.0.2
* Flask-Bootstrap==3.3.7.1
* Flask-WTF==0.14.2
* WTForms==2.2.1
* requests==2.18.4
* pymongo==3.8.0
* jsonschema==2.6.0

**I'm using two libs my own, to consume the API and LOG.
I put them in the same project, so I did not have to download them via `git+ssh`

* consumerapi.py (Not available on github)
* lib_formatter_logger - https://github.com/ErickSystem/lib_formatter_logger.git

### FRONT

* jquery.tablesorter.min.js: 2.0
* jquery-3.3.1.slim.min.js: 3.3.1
* popper.min.js
* bootstrap: 4.1.3

## EXECUTION

Run the command below

    `make upd`

The App will be available at:

    `http://localhost:8080`

View Application Log

```bash
    docker logs --tail 100 -f challenge_mutant_app_1
```

** If you are using Linux and have JQ installed, use the command below to view the application logs inside the container

```base
    docker logs --tail 100 -f challenge_mutant_app_1 | grep "^{" | jq .
```